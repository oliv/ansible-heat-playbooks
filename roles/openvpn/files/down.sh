#!/bin/bash
FORWARDING=$(cat /etc/openvpn/net.ipv4.conf.all.forwarding.bak)
echo "restoring net.ipv4.conf.all.forwarding=$FORWARDING"
/sbin/sysctl net.ipv4.conf.all.forwarding=$FORWARDING
/etc/openvpn/fw.stop
echo "Restoring iptables"
/sbin/iptables-restore < /etc/openvpn/iptables.save