#!/bin/bash
/sbin/sysctl -n net.ipv4.conf.all.forwarding > /etc/openvpn/net.ipv4.conf.all.forwarding.bak
/sbin/sysctl net.ipv4.conf.all.forwarding=1
/sbin/iptables-save > /etc/openvpn/iptables.save
/sbin/iptables -t nat -F
/sbin/iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -j MASQUERADE